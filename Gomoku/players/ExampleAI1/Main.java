import java.util.*;

public class Main {
	private static final Random random = new Random();
	
	public static void main(String[] args) {
		String c;
		String[][] map = new String[19][19];
		Scanner s = new Scanner(System.in);
		while (true) {
			c = s.next();
			int pcount = 0;
			String response = "";
			for (int i = 0; i < 19; ++i) {
				for (int j = 0; j < 19; ++j) {
					map[i][j] = s.next();
					if (!("X".equals(map[i][j]))) {
						pcount++;
					}
				}
			}
			List<Coord> coords = extractEmptyCoords(map);
			response = coordsToString(getRandomCoord(coords));
			
			System.out.println(response);
		}
	}
	
	private static List<Coord> extractEmptyCoords(String[][] map) {
		List<Coord> result = new ArrayList<>(19*19);
		for (int i = 0; i < 19; ++i) {
			for (int j = 0; j < 19; ++j) {
				if ("X".equals(map[i][j])) {
					result.add(new Coord(i, j));
				}
			}
		}
		return result;
	}
	
	private static Coord getRandomCoord(List<Coord> coords) {
		int i = random.nextInt(coords.size());
		Coord result = coords.get(i);
		coords.remove(result);
		return result;
	}
	
	private static String coordsToString(Coord... coords) {
		String result = "";
		int i = 0;
		for (Coord c : coords) {
			result += "" + c.x + " " + c.y;
			if (++i != coords.length) {
				result += " ";
			}
		}
		return result;
	}
}

class Coord {
	public int x;
	public int y;
	
	public Coord(int x, int y) {
		this.x = x;
		this.y = y;
	}
}
