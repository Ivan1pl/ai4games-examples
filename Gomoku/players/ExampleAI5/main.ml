let rec skipl() = let c = read_line() in if c = "W" || c = "B" then () else skipl()

let initl n f = let rec initl_i n = if n = 0 then [] else (f ()) :: initl_i (n-1) in List.rev (initl_i n)

let split s = let l = ref [] in let () = String.iter (fun c -> if c <> ' ' then l := c::(!l) else ()) s in List.rev !l

let read_map() = initl 19 (fun () -> split (read_line()))

let get_coords map =
  let rec get_coords_row row x y =
    match row with
    | []     -> []
    | h :: t -> let l = get_coords_row t x (y+1) in if h = 'X' then (x, y) :: l else l
  in let rec get_coords_i map i =
    match map with
    | []      -> []
    | x :: xs -> get_coords_row x i 0 @ get_coords_i xs (i+1)
  in get_coords_i map 0

let choose_move map = let coords = get_coords map in let r = Random.int (List.length coords) in List.nth coords r

let print_move (x, y) = print_endline ((string_of_int x) ^ " " ^ (string_of_int y))

let rec main() = skipl(); let map = read_map() in print_move (choose_move map); main()

let () = Random.self_init(); main()

