#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct {
    int x;
    int y;
} coord;

char map[19][19];
coord coords[19*19];

char read_char() {
    char c = 0;
    while (c != 'X' && c != 'W' && c != 'B') {
        scanf("%c",&c);
    }
    return c;
}

int main() {
    srand(time(NULL));
    char c;
    while (1) {
        c = read_char();
        int ecount = 0;
        for (int i = 0; i < 19; ++i) {
            for (int j = 0; j < 19; ++j) {
                map[i][j] = read_char();
                if (map[i][j] == 'X') {
                    coords[ecount].x = i;
                    coords[ecount].y = j;
                    ecount++;
                }
            }
        }
        int r = rand()%ecount;
        printf("%d %d\n", coords[r].x, coords[r].y);
        fflush(stdout);
    }
    return 0;
}

