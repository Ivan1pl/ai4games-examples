from sys import *
import random

random.seed()

while True:
    c = stdin.readline()
    while c != 'W\n' and c != 'B\n':
        c = stdin.readline()
    m = list()
    for i in range(0, 19):
        m.append(stdin.readline().split())
    empty = list()
    e = 0
    for i in range(0, 19):
        for j in range(0, 19):
            if m[i][j] == 'X':
                empty.append((i, j))
                e = e+1
    r = random.randint(0, e-1)
    x, y = empty[r]
    print(x, y)
    stdout.flush()

