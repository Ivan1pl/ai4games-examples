#include <iostream>
#include <cstdlib>
#include <ctime>

class Coord {
    public:
        int x;
        int y;
};

char map[19][19];
Coord coords[19*19];

char read_char() {
    char c;
    std::cin >> std::skipws >> c;
    return c;
}

int main() {
    srand(time(NULL));
    char c;
    while (true) {
        c = read_char();
        int ecount = 0;
        for (int i = 0; i < 19; ++i) {
            for (int j = 0; j < 19; ++j) {
                map[i][j] = read_char();
                if (map[i][j] == 'X') {
                    coords[ecount].x = i;
                    coords[ecount].y = j;
                    ecount++;
                }
            }
        }
        int r = rand()%ecount;
        std::cout << coords[r].x << " " << coords[r].y << std::endl;
    }
    return 0;
}

