import com.ivan1pl.ai4games.entity.PlayerAIStatus;
import com.ivan1pl.ai4games.ipc.GameResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIResponseMessage;
import com.ivan1pl.ai4games.ipc.PlayerAIStatusField;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import javax.swing.*;

public class Main {
    
    public static void main(String[] args) {
        if (args.length == 1 || args.length == 2) {
            String playerName = args.length == 2 ? args[1] : null;
            new ReplayWindow(new File(args[0]), playerName);
        } else {
            Gomoku game = new Gomoku();
            int playerId = 0;
            Scanner inScanner = new Scanner(System.in);
            PlayerAIResponseMessage initMessage = PlayerAIResponseMessage.deserialize(inScanner.nextLine());
            System.out.println(prepareOutput(game, GameResult.NONE, 0, initMessage.players.get(0), initMessage.players.get(1)));
            while (true) {
                GameResult result;
                PlayerAIResponseMessage playerMessage = PlayerAIResponseMessage.deserialize(inScanner.nextLine());
                PlayerAIStatusField f1 = playerMessage.players.get(0);
                PlayerAIStatusField f2 = playerMessage.players.get(1);
                if ((f1.status == PlayerAIStatus.OUT || f1.status == PlayerAIStatus.LOST) && (f2.status == PlayerAIStatus.OUT || f2.status == PlayerAIStatus.LOST)) {
                    result = GameResult.DRAW;
                } else if (f1.status == PlayerAIStatus.TIE && f2.status == PlayerAIStatus.TIE) {
                    result = GameResult.DRAW;
                } else if (f1.status == PlayerAIStatus.OUT || f1.status == PlayerAIStatus.LOST) {
                    result = GameResult.PLAYER2;
                } else if (f2.status == PlayerAIStatus.OUT || f2.status == PlayerAIStatus.LOST) {
                    result = GameResult.PLAYER1;
                } else if (f1.status != PlayerAIStatus.ALIVE || f2.status != PlayerAIStatus.ALIVE) {
                    result = GameResult.DRAW;
                } else {
                    playerId = getPlayerIdFromMessage(playerMessage);
                    String input = playerMessage.playerOutput;
                    try {
                        result = game.playerMove(playerId, input);
                    } catch (Exception e) {
                        if (playerId == 0) {
                            result = GameResult.PLAYER2;
                        } else {
                            result = GameResult.PLAYER1;
                        }
                    }
                }
                System.out.println(prepareOutput(game, result, (playerId+1)%2, f1, f2));
            }
        }
    }
    
    private static int getPlayerIdFromMessage(PlayerAIResponseMessage message) {
        String currentPlayer = message.currentPlayer;
        int i = 0;
        for (PlayerAIStatusField f : message.players) {
            if (f.id.equals(currentPlayer)) {
                return i;
            }
            i++;
        }
        return -1;
    }
    
    private static String prepareOutput(Gomoku game, GameResult result, int playerId, PlayerAIStatusField player1, PlayerAIStatusField player2) {
        String playerName = playerId == 0 ? player1.id : player2.id;
        switch (result) {
            case NONE:
                player1.status = PlayerAIStatus.ALIVE;
                player2.status = PlayerAIStatus.ALIVE;
                break;
            case DRAW:
                player1.status = PlayerAIStatus.TIE;
                player2.status = PlayerAIStatus.TIE;
                break;
            case PLAYER1:
                player1.status = PlayerAIStatus.WON;
                player2.status = PlayerAIStatus.LOST;
                break;
            case PLAYER2:
                player1.status = PlayerAIStatus.LOST;
                player2.status = PlayerAIStatus.WON;
                break;
        }
        GameResponseMessage message = new GameResponseMessage();
        ArrayList<PlayerAIStatusField> l = new ArrayList<>(2);
        l.add(player1);
        l.add(player2);
        message.players = l;
        message.currentPlayer = playerName;
        message.playerInput = game.stateToString(playerId);
        return message.serialize();
    }
}

class Gomoku {
    private static final int SIZE = 19;
    private final char[][] board;
    private char[] playerColor = {'B', 'W'};
    private int pieces = 0;
    
    public Gomoku() {
        board = new char[SIZE][SIZE];
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                board[i][j] = 'X';
            }
        }
    }
    
    public String stateToString(int playerId) {
        String result = "";
        result += playerColor[playerId];
        result += "\n";
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                result += board[i][j] + " ";
            }
            result += "\n";
        }
        return result;
    }
    
    public GameResult playerMove(int playerId, String input) throws InvalidMoveException {
        Scanner s = new Scanner(input);
        
        int x, y;
        x = s.nextInt();
        y = s.nextInt();
        set(x, y, playerColor[playerId]);
        
        if (pieces == SIZE * SIZE) {
            return GameResult.DRAW;
        } else if (checkWin(0)) {
            return GameResult.PLAYER1;
        } else if (checkWin(1)) {
            return GameResult.PLAYER2;
        }
        return GameResult.NONE;
    }
    
    private void set(int x, int y, char c) throws InvalidMoveException {
        if (board[x][y] == 'X') {
            board[x][y] = c;
            pieces++;
        } else {
            throw new InvalidMoveException();
        }
    }
    
    private boolean checkWin(int playerId) {
        char c = playerColor[playerId];
        
        return findSequence(c, 0, 0, 1, 0, 5) || findSequence(c, 0, 0, 0, 1, 5) ||
                findSequence(c, 0, 0, 1, 1, 5) || findSequence(c, 4, 0, -1, 1, 5);
    }
    
    private boolean findSequence(char color, int startX, int startY, int stepX, int stepY, int length) {
        for (int i = startX; i < Math.min(SIZE-(length-1)*stepX, SIZE); i++) {
            for (int j = startY; j < Math.min(SIZE-(length-1)*stepY, SIZE); j++) {
                boolean found = true;
                for (int k = 0; k < length; ++k) {
                    if (board[i + k*stepX][j + k*stepY] != color) {
                        found = false;
                    }
                }
                if (found) {
                    return true;
                }
            }
        }
        return false;
    }
}

enum GameResult {
    NONE,
    DRAW,
    PLAYER1,
    PLAYER2;
}

class InvalidMoveException extends Exception {
    public InvalidMoveException() {
        super("Invalid move");
    }
}

class ReplayWindow extends JFrame implements ActionListener {
    private static final int SIZE = 19;
    private static final String ACTION_PREV = "PREV";
    private static final String ACTION_NEXT = "NEXT";
    
    private JToolBar toolBar;
    private JLabel roundLabel;
    private JButton prevButton;
    private JButton nextButton;
    private GameStateCanvas canvas;
    private PlayerDataPanel panel;
    
    private final String debugPlayer;
    
    private int currentRound = 0;
    private final int minRound = 0;
    private int maxRound;
    
    private final List<PlayerAIResponseMessage> playerMessages = new ArrayList<>();
    private final List<GameResponseMessage> gameMessages = new ArrayList<>();
    
    public ReplayWindow(File logFile, String debugPlayer) {
        super("Gomoku");
        this.debugPlayer = debugPlayer;
        
        readLogs(logFile);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout(new BorderLayout());
        
        canvas = new GameStateCanvas(null);
        panel = new PlayerDataPanel();
        toolBar = makeToolbar();
        add(toolBar, BorderLayout.PAGE_START);
        
        add(canvas, BorderLayout.CENTER);
        add(panel, BorderLayout.LINE_END);
        drawRound();
        
        pack();
        
        setVisible(true);
    }
    
    private void readLogs(File f) {
        try {
            Scanner s = new Scanner(f);
            if (s.hasNextLine()) {
                s.nextLine();
            }
            while (s.hasNextLine()) {
                gameMessages.add(GameResponseMessage.deserialize(s.nextLine()));
                if (s.hasNextLine()) {
                    playerMessages.add(PlayerAIResponseMessage.deserialize(s.nextLine()));
                }
            }
            s.close();
        } catch (Exception e) { }
        
        maxRound = gameMessages.size() - 1;
    }
    
    private JToolBar makeToolbar() {
        toolBar = new JToolBar("Control bar");
        toolBar.setFloatable(false);
        addToolBarElements(toolBar);
        return toolBar;
    }
    
    private void addToolBarElements(JToolBar toolBar) {
        prevButton = new JButton("<");
        prevButton.setActionCommand(ACTION_PREV);
        prevButton.setToolTipText("Previous turn");
        prevButton.addActionListener(this);
        toolBar.add(prevButton);
        
        roundLabel = new JLabel();
        updateLabel();
        toolBar.add(roundLabel);
        
        nextButton = new JButton(">");
        nextButton.setActionCommand(ACTION_NEXT);
        nextButton.setToolTipText("Next turn");
        nextButton.addActionListener(this);
        toolBar.add(nextButton);
        
        updateRound(0);
    }
    
    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        
        if (ACTION_PREV.equals(cmd)) {
            updateRound(-1);
            updateLabel();
        } else if (ACTION_NEXT.equals(cmd)) {
            updateRound(1);
            updateLabel();
        }
    }
    
    private void updateRound(int step) {
        if (currentRound + step >= minRound && currentRound + step <= maxRound) {
            currentRound += step;
        }
        
        if (currentRound <= minRound) {
            prevButton.setEnabled(false);
        } else {
            prevButton.setEnabled(true);
        }
        
        if (currentRound >= maxRound) {
            nextButton.setEnabled(false);
        } else {
            nextButton.setEnabled(true);
        }
        
        drawRound();
    }
    
    private void updateLabel() {
        roundLabel.setText(Integer.toString(currentRound));
    }
    
    private void drawRound() {
        Scanner s = new Scanner(gameMessages.get(currentRound).playerInput);
        s.next();
        String[][] map = new String[SIZE][SIZE];
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                map[i][j] = s.next();
            }
        }
        canvas.update(map);
        String playerName = null;
        String playerOutput = null;
        String playerError = null;
        if (currentRound > 0) {
            PlayerAIResponseMessage playerMessage = playerMessages.get(currentRound - 1);
            playerName = playerMessage.currentPlayer;
            playerOutput = playerMessage.playerOutput;
            playerError = playerMessage.playerDebugOutput;
        }
        if (playerName != null && debugPlayer != null && !playerName.equals(debugPlayer)) {
            playerError = null;
        }
        panel.update(playerName, playerOutput, playerError);
    }
}

class GameStateCanvas extends JComponent {
    private static final int SIZE = 19;
    private String[][] map;

    public GameStateCanvas(String[][] map) {
        super();
        setPreferredSize(new Dimension(570, 570));
        setBackground(Color.DARK_GRAY);
        this.map = map;
    }
    
    public void update(String[][] map) {
        this.map = map;
        repaint();
    }
    
    public void paintComponent(Graphics g) {
        g.setColor(Color.DARK_GRAY);
        g.fillRect(0, 0, 570, 570);
        g.setColor(Color.BLACK);
        drawGrid(g);
        if (map != null) {
            drawPieces(g);
        }
    }
    
    private void drawGrid(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        for (int i = 1; i < SIZE; ++i) {
            Line2D lin = new Line2D.Float(i*30, 0, i*30, 570);
            g2.draw(lin);
            lin = new Line2D.Float(0, i*30, 570, i*30);
            g2.draw(lin);
        }
    }
    
    private void drawPieces(Graphics g) {
        for (int i = 0; i < SIZE; ++i) {
            for (int j = 0; j < SIZE; ++j) {
                Color c = Color.DARK_GRAY;
                if ("W".equals(map[i][j])) {
                    c = Color.WHITE;
                } else if ("B".equals(map[i][j])) {
                    c = Color.BLACK;
                }
                drawPiece(g, i*30 + 15, j*30 + 15, c);
            }
        }
    }
    
    private void drawPiece(Graphics g, int centerX, int centerY, Color fill) {
        g.setColor(fill);
        g.fillOval(centerX - 12, centerY - 12, 24, 24);
        g.setColor(Color.BLACK);
        g.drawOval(centerX - 12, centerY - 12, 24, 24);
    }
}

class PlayerDataPanel extends JPanel {
    private JLabel currentPlayerLabel;
    private InfoTextArea outputTextArea;
    private InfoTextArea debugTextArea;

    public PlayerDataPanel() {
        super();
        setPreferredSize(new Dimension(300, 570));
        
        Box box = Box.createVerticalBox();
        JLabel label = new JLabel("Current player:");
        label.setHorizontalTextPosition(JLabel.LEFT);
        box.add(label);
        currentPlayerLabel = new JLabel();
        currentPlayerLabel.setForeground(Color.RED);
        currentPlayerLabel.setHorizontalTextPosition(JLabel.LEFT);
        box.add(currentPlayerLabel);
        label = new JLabel("Player output:");
        label.setHorizontalTextPosition(JLabel.LEFT);
        box.add(label);
        outputTextArea = new InfoTextArea();
        box.add(outputTextArea);
        label = new JLabel("Player debug:");
        label.setHorizontalTextPosition(JLabel.LEFT);
        box.add(label);
        debugTextArea = new InfoTextArea();
        box.add(debugTextArea);
        add(box);
    }
    
    public void update(String playerName, String playerOutput, String playerError) {
        currentPlayerLabel.setText(playerName);
        outputTextArea.setText(playerOutput);
        debugTextArea.setText(playerError);
    }
}

class InfoTextArea extends JTextArea {
    public InfoTextArea() {
        super();
        setPreferredSize(new Dimension(290, 240));
        setEnabled(false);
        setAlignmentX(Component.LEFT_ALIGNMENT);
        setDisabledTextColor(Color.BLACK);
    }
}

